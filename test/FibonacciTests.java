package test;

import java.util.Random;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import main.Fibonacci;

public class FibonacciTests {
	public Fibonacci fi=new Fibonacci();
	
	@Test
	public void testFibonacci() {
		try {
			Assertions.assertEquals(0, fi.F(0));
			Assertions.assertEquals(1, fi.F(1));
			Assertions.assertEquals(1, fi.F(2));
			Assertions.assertEquals(2, fi.F(3));
			Assertions.assertEquals(3, fi.F(4));
			Assertions.assertEquals(5, fi.F(5));
			Assertions.assertThrows(ArithmeticException.class, ()->fi.F(-1));
			Assertions.assertThrows(ArithmeticException.class, ()->fi.F(Integer.MIN_VALUE));
			Assertions.assertThrows(ArithmeticException.class, ()->fi.F(Integer.MAX_VALUE));
			int x = new Random().nextInt();
			if(x<0) {
				Assertions.assertThrows(ArithmeticException.class, ()->fi.F(x));
			}
			else {
				int res=0, ret1=0, ret2=1;
				for(int i=0; i<x; i++) {
					if(i==0) {
						res=0;
					}
					if(i==1) {
						res=1;
					}
					else {
						res=ret1+ret2;
						ret1=ret2;
						ret2=res;
					}
					Assertions.assertEquals(res, fi.F(x));
				}	
			}
		} catch (Exception e) {}
	}
}