package test.morpion;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import main.morpion.Grid;

public class GridTests {
	public Grid gr = new Grid();
	
	@BeforeEach
	public void Reset() {
		gr.grid[0][0]="A1";
		gr.grid[0][1]="A2";
		gr.grid[0][2]="A3";
		gr.grid[1][0]="B1";
		gr.grid[1][1]="B2";
		gr.grid[1][2]="B3";
		gr.grid[2][0]="C1";
		gr.grid[2][1]="C2";
		gr.grid[2][2]="C3";
	}
	
	@Test
	public void testGridA1() {
		try {
			gr.insert("A1");
		} catch (Exception e) {}
		Assertions.assertEquals("X", gr.getString(0, 0));
	}
	
	@Test
	public void testDoubleInsertA1() {
		try {
			gr.insert("A1");
		} catch (Exception e) {}
		Assertions.assertThrows(Exception.class, ()->gr.insert("A1"));
	}
	
	@Test
	public void testGridA2() {
		try {
			gr.insert("A2");
		} catch (Exception e) {}
		Assertions.assertEquals("X", gr.getString(0, 1));
	}
	
	@Test
	public void testDoubleInsertA2() {
		try {
			gr.insert("A2");
		} catch (Exception e) {}
		Assertions.assertThrows(Exception.class, ()->gr.insert("A2"));
	}
	
	@Test
	public void testGridA3() {
		try {
			gr.insert("A3");
		} catch (Exception e) {}
		Assertions.assertEquals("X", gr.getString(0, 2));
	}
	
	@Test
	public void testDoubleInsertA3() {
		try {
			gr.insert("A3");
		} catch (Exception e) {}
		Assertions.assertThrows(Exception.class, ()->gr.insert("A3"));
	}
			
	@Test
	public void testGridB1() {
		try {
			gr.insert("B1");
		} catch (Exception e) {}
		Assertions.assertEquals("X", gr.getString(1, 0));
	}
	
	@Test
	public void testDoubleInsertB1() {
		try {
			gr.insert("B1");
		} catch (Exception e) {}
		Assertions.assertThrows(Exception.class, ()->gr.insert("B1"));
	}
	
	@Test
	public void testGridB2() {			
		try {
			gr.insert("B2");
		} catch (Exception e) {}
		Assertions.assertEquals("X", gr.getString(1, 1));
	}
	
	@Test
	public void testDoubleInsertB2() {
		try {
			gr.insert("B2");
		} catch (Exception e) {}
		Assertions.assertThrows(Exception.class, ()->gr.insert("B2"));
	}
		
	@Test
	public void testGridB3() {
		try {
			gr.insert("B3");
		} catch (Exception e) {}
		Assertions.assertEquals("X", gr.getString(1, 2));
	}
	
	@Test
	public void testDoubleInsertB3() {
		try {
			gr.insert("B3");
		} catch (Exception e) {}
		Assertions.assertThrows(Exception.class, ()->gr.insert("B3"));
	}
			
	@Test
	public void testGridC1() {
		try {
			gr.insert("C1");
		} catch (Exception e) {}
		Assertions.assertEquals("X", gr.getString(2, 0));
	}
	
	@Test
	public void testDoubleInsertC1() {
		try {
			gr.insert("C1");
		} catch (Exception e) {}
		Assertions.assertThrows(Exception.class, ()->gr.insert("C1"));
	}
			
	@Test
	public void testGridC2() {
		try {
			gr.insert("C2");
		} catch (Exception e) {}
		Assertions.assertEquals("X", gr.getString(2, 1));
	}
	
	@Test
	public void testDoubleInsertC2() {
		try {
			gr.insert("C2");
		} catch (Exception e) {}
		Assertions.assertThrows(Exception.class, ()->gr.insert("C2"));
	}
			
	@Test
	public void testGridC3() {
		try {
			gr.insert("C3");
		} catch (Exception e) {}
		Assertions.assertEquals("X", gr.getString(2, 2));
	}
	
	@Test
	public void testDoubleInsertC3() {
		try {
			gr.insert("C3");
		} catch (Exception e) {}
		Assertions.assertThrows(Exception.class, ()->gr.insert("C3"));
	}
			
	@Test
	public void testGridLetterO() {
		gr.lettre="O";
		try {
			gr.insert("C3");
		} catch (Exception e) {}
		Assertions.assertEquals("O", gr.getString(2, 2));
	}
			
	@Test
	public void testOutOfGrid() {
		Assertions.assertThrows(Exception.class, ()->gr.insert("D7"));
	}
	
	@Test
	public void testWinVertical() {
		try {
			gr.insert("A3");
			gr.insert("A2");
			gr.insert("B3");
			gr.insert("B2");
			gr.insert("C3");
		} catch (Exception e) {}
		Assertions.assertEquals(true, gr.isWin());
	}
	
	@Test
	public void testWinHorizontal() {
		try {
			gr.insert("C1");
			gr.insert("B1");
			gr.insert("C2");
			gr.insert("B2");
			gr.insert("C3");
		} catch (Exception e) {}
		Assertions.assertEquals(true, gr.isWin());
	}
	
	@Test
	public void testWinDiagonal() {
		try {
			gr.insert("A1");
			gr.insert("A2");
			gr.insert("B2");
			gr.insert("B3");
			gr.insert("C3");
		} catch (Exception e) {}
		Assertions.assertEquals(true, gr.isWin());
	}
	
	@Test
	public void testLose() {
		try {
			gr.insert("A1");
			gr.insert("A2");
		} catch (Exception e) {}
		Assertions.assertEquals(false, gr.isWin());
	}
	
	@Test
	public void testRandom() {
		boolean res=false;
		List<String> list = new ArrayList<>();
		Random rand = new Random();
		for (Integer j=0; j<=9; j++) {
			Integer x=rand.nextInt(3);
			Integer y=rand.nextInt(3);
			if (x==0 && y ==0) {
				list.add("A1");
			}
			if (x==0 && y ==1) {
				list.add("A2");
			}
			if (x==0 && y ==2) {
				list.add("A3");
			}
			if (x==1 && y ==0) {
				list.add("B1");
			}
			if (x==1 && y ==1) {
				list.add("B2");
			}
			if (x==1 && y ==2) {
				list.add("B3");
			}
			if (x==2 && y ==0) {
				list.add("C1");
			}
			if (x==2 && y ==1) {
				list.add("C2");
			}
			if (x==2 && y ==2) {
				list.add("C3");
			}
		}
		try {
			for (String i : list){
				gr.insert(i);
			}
			for (Integer j=0; j<3; j++) {
				if(gr.grid[j][0]==gr.grid[j][1] && gr.grid[j][1]==gr.grid[j][2]) {
					res=true;
				}
				if(gr.grid[0][j]==gr.grid[1][j] && gr.grid[1][j]==gr.grid[2][j]) {
					res=true;
				}
			}
			if((gr.grid[0][0]==gr.grid[1][1] && gr.grid[1][1]==gr.grid[2][2]) || (gr.grid[0][2]==gr.grid[1][1] && gr.grid[1][1]==gr.grid[2][0])) {
				res=true;
			}
		} catch (Exception e) {}
		Assertions.assertEquals(res, gr.isWin());
	}
}