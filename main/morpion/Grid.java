package main.morpion;

public class Grid {
	public String[][] grid = new String[3][3];
	public String lettre = "X";

	public void insert(String pos) throws Exception {
		if(pos=="A1") {
			if(grid[0][0]=="A1") {
				grid[0][0]=lettre;
			}
			else {
				throw new Exception();
			}
		}
		else if(pos=="A2") {
			if(grid[0][1]=="A2") {
				grid[0][1]=lettre;
			}
			else {
				throw new Exception();
			}
		}
		else if(pos=="A3") {
			if(grid[0][2]=="A3") {
				grid[0][2]=lettre;
			}
			else {
				throw new Exception();
			}
		}
		else if(pos=="B1") {
			if(grid[1][0]=="B1") {
				grid[1][0]=lettre;
			}
			else {
				throw new Exception();
			}
		}
		else if(pos=="B2") {
			if(grid[1][1]=="B2") {
				grid[1][1]=lettre;
			}
			else {
				throw new Exception();
			}
		}
		else if(pos=="B3") {
			if(grid[1][2]=="B3") {
				grid[1][2]=lettre;
			}
			else {
				throw new Exception();
			}
		}
		else if(pos=="C1") {
			if(grid[2][0]=="C1") {
				grid[2][0]=lettre;
			}
			else {
				throw new Exception();
			}
		}
		else if(pos=="C2") {
			if(grid[2][1]=="C2") {
				grid[2][1]=lettre;
			}
			else {
				throw new Exception();
			}
		}
		else if(pos=="C3") {
			if(grid[2][2]=="C3") {
				grid[2][2]=lettre;
			}
			else {
				throw new Exception();
			}
		}
		else {
			throw new Exception();
		}
		if(lettre == "X") {
			lettre = "O";
		}
		else {
			lettre = "X";
		}
	}

	public boolean isWin() {
		if(grid[0][2]==grid[1][2] && grid[1][2]==grid[2][2]) {
			return true;
		}
		if(grid[2][0]==grid[2][1] && grid[2][1]==grid[2][2]) {
			return true;
		}
		if(grid[0][0]==grid[1][1] && grid[1][1]==grid[2][2]) {
			return true;
		}
		for (Integer j=0; j<3; j++) {
			if(grid[j][0]==grid[j][1] && grid[j][1]==grid[j][2]) {
				return true;
			}
			if(grid[0][j]==grid[1][j] && grid[1][j]==grid[2][j]) {
				return true;
			}
		}
		if((grid[0][0]==grid[1][1] && grid[1][1]==grid[2][2]) || (grid[0][2]==grid[1][1] && grid[1][1]==grid[2][0])) {
			return true;
		}
		return false;
	}
	
	public String getString(Integer x, Integer y) {
		return grid[x][y];
	}
}