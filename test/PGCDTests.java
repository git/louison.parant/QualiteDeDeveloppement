package test;

import java.util.Random;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import main.PGCD;

public class PGCDTests {
	public PGCD pgcd = new PGCD();
	
	@Test
	public void testPGCD() {
		try {
			Assertions.assertEquals(0, pgcd.pgcd(0, 0));
			Assertions.assertEquals(12, pgcd.pgcd(96, 36));
			Assertions.assertEquals(12, pgcd.pgcd(36, 96));
			Assertions.assertEquals(2, pgcd.pgcd(4, -2));
			Assertions.assertEquals(2, pgcd.pgcd(-4, 2));
			Assertions.assertEquals(2, pgcd.pgcd(-2, -2));
			Assertions.assertThrows(ArithmeticException.class, ()->pgcd.pgcd(Integer.MIN_VALUE, Integer.MIN_VALUE));
			Assertions.assertThrows(ArithmeticException.class, ()->pgcd.pgcd(Integer.MAX_VALUE, Integer.MAX_VALUE));
			int x = new Random().nextInt(), y = new Random().nextInt();
			int res=0, min;
			if(x==0 && y==0) {
				res=0;
			}
			else {
				if(x<=y) {
					min=x;
				}
				else {
					min=y;
				}
				for (int i=1; i<min; i++) {
					if(x%i==0 && y%i==0) {
						res=i;
					}
				}
			}
			Assertions.assertEquals(res, pgcd.pgcd(x, y));
		} catch(Exception e) {}
	}
}