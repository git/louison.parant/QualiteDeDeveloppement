package main;

public class Fibonacci {

	public Integer F(int i) throws Exception{
		if(i==0) {
			return 0;
		}
		if(i==1) {
			return 1;
		}
		if(i<0) {
			throw new ArithmeticException();
		}
		if(i>100) {
			throw new ArithmeticException();
		}
		{
		int res=0, ret1=0, ret2=1;
		for(int j=2; j<=i; j++) {
			res=ret1+ret2;
			ret1=ret2;
			ret2=res;
		}
		return res;
		}
	}
}