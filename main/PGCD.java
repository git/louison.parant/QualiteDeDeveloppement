package main;

public class PGCD {

	public Integer pgcd(int x, int y) throws Exception{
		int res=0, min;
		if(x>100 || x<-100 || y>100 || y<-100) {
			throw new ArithmeticException();
		}
		if(x<0) {
			x=0-x;
		}
		if(y<0) {
			y=0-y;
		}
		if(x==0 && y==0) {
			res=0;
		}
		else {
			if(x<=y) {
				min=x;
			}
			else {
				min=y;
			}
			for (int i=1; i<=min; i++) {
				if(x%i==0 && y%i==0) {
					res=i;
				}
			}
		}
		return res;
	}
}