package test;

import java.util.Random;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import main.Pythagore;

public class PythagoreTests {
	public Pythagore py = new Pythagore();
	
	@Test
	public void testPythagore() {
		try {
			Assertions.assertEquals(false, py.P(0, 0, 0));
			Assertions.assertEquals(false, py.P(-2, 2, 2));
			Assertions.assertEquals(false, py.P(2, -2, 2));
			Assertions.assertEquals(false, py.P(2, 2, -2));
			Assertions.assertEquals(false, py.P(2, 2, 2));
			Assertions.assertEquals(true, py.P(3, 4, 5));
			Assertions.assertEquals(true, py.P(4, 5, 3));
			Assertions.assertEquals(true, py.P(5, 3, 4));
			
			Assertions.assertEquals(false, py.P(Integer.MIN_VALUE, Integer.MIN_VALUE, Integer.MIN_VALUE));
			Assertions.assertThrows(ArithmeticException.class, ()->py.P(Integer.MAX_VALUE, Integer.MAX_VALUE, Integer.MAX_VALUE));
			
			int x = new Random().nextInt(), y = new Random().nextInt(), z = new Random().nextInt();
			boolean res = false;
			
			if (x<0 || y<0 || y<0) {
				res = false;
			}
			else if((x*x)>=Integer.MAX_VALUE || (y*y)>=Integer.MAX_VALUE || (z*z)>=Integer.MAX_VALUE || (x*x)<0 || (y*y)<0 || (z*z)<0) {
				res = false;
			}
			else if((x*x)+(y*y)!=(z*z) && (x*x)+(z*z)!=(y*y) && (y*y)+(z*z)!=(x*x)) {
				res = false;
			}
			else {
				res = true;
			}
			
			Assertions.assertEquals(res, py.P(x, y, z));
			
		} catch(Exception e) {}
	}
}