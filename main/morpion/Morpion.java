package main.morpion;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner; 

public class Morpion {
	public Integer tour = 0;
	public Grid grille = new Grid();
	public List<String> list = new ArrayList<>();
	
	public void jouer() throws Exception {
		Scanner sc = new Scanner(System.in);
		list.add("A1");
		list.add("A2");
		list.add("A3");
		list.add("B1");
		list.add("B2");
		list.add("B3");
		list.add("C1");
		list.add("C2");
		list.add("C3");
		while(tour < 4) {
			if(tour==4) {
				grille.insert(list.get(0));
			}
			else {
				afficherGrille();
				System.out.println("Joueur 1, select an available slot : ");
				String choice = sc.nextLine();
				grille.insert(choice);
				if(testWin(1)) {
					System.out.println("Joueur 1 a gagné");
					sc.close();
					return;
				}
				afficherGrille();
				System.out.println("Joueur 2, select an available slot : ");
				choice = sc.nextLine();
				grille.insert(choice);
				if(testWin(2)) {
					System.out.println("Joueur 2 a gagné");
					sc.close();
					return;
				}
			}
		}
		System.out.println("Égalité");
		sc.close();
	}

	private boolean testWin(int i) {
		if(tour < 3) {
			return false;
		}
		return grille.isWin();
	}

	public void afficherGrille() {
		for(Integer i=0; i<3; i++) {
			System.out.println(grille.grid[i][0] + " | " + grille.grid[i][1] + " | " + grille.grid[i][2]);
		}
	}
}