package main;

public class Pythagore {
	public boolean P(int x, int y, int z) {
		if(x<-100 || y<-100 || z<-100) {
			return false;
		}
		if(x>100 || y>100 || z>100) {
			throw new ArithmeticException();
		}
		if(x<=0 || y<=0 || y<=0) {
			return false;
		}
		if((x*x)>=Integer.MAX_VALUE || (y*y)>=Integer.MAX_VALUE || (z*z)>=Integer.MAX_VALUE || (x*x)<0 || (y*y)<0 || (z*z)<0) {
			return false;
		}
		if((x*x)+(y*y)!=(z*z) && (x*x)+(z*z)!=(y*y) && (y*y)+(z*z)!=(x*x)) {
			return false;
		}
		return true;
	}
}